import { useShipStore } from "../components/Ship";

export function cannonballFactory(last: number) {
  let angle;
  do {
    angle = Math.random() * Math.PI * 2;
  } while (angle > last - Math.PI / 2 && angle < last + Math.PI / 2);
  return {
    angle,
    radius: 0,
    size: 1,
    collision: false,
  };
}

export function calculatePlayerSpeed(stage: number) {
  const BASE = 1.35;
  const ACCELERATION = 0.05;
  return (BASE + stage * ACCELERATION) / 1000;
}

export function calculateCannonSpeed(stage: number) {
  const BASE = 750;
  const ACCELERATION = 7.5;
  return BASE / (1 + stage / ACCELERATION);
}

export function calculateCannonballSpeed(stage: number) {
  const BASE = 35;
  const ACCELERATION = 0.5;
  return (BASE + stage * ACCELERATION) / 1000;
}

export function displayGameOverModal(message: string) {
  const modal = document.getElementById("gameOverModal");
  const modalMessage = document.getElementById("gameOverMessage");

  useShipStore.setState({ shipDestroyed: true });
  modal!.style.display = "block";
  modalMessage!.textContent = message;
}

export function hideGameOverModal() {
  const modal = document.getElementById("gameOverModal");
  const locExplosion = document.getElementById("locExplosion");

  useShipStore.setState({ shipDestroyed: false });
  locExplosion!.innerHTML = "";
  modal!.style.display = "none";
}

export function explosion() {
  const listExplosion = [
    {
      top: "6%",
      left: "42%",
    },
    {
      top: "30%",
      left: "26%",
    },
    {
      top: "30%",
      left: "59%",
    },
    {
      top: "64%",
      left: "42%",
    },
  ];
  const locExplosion = document.getElementById("locExplosion");

  listExplosion.forEach((item) => {
    const explosion1 = `
        <div style="position: absolute; top: ${item.top}; left: ${item.left};">
            <lottie-player src="https://assets5.lottiefiles.com/private_files/lf30_zzhwy8ge.json"  background="transparent"  speed="1"  style="width: 300px; height: 300px;" autoplay></lottie-player>
        </div>`;
    locExplosion!.innerHTML += explosion1;
  });
}
