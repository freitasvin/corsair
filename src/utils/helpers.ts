import { Vector2 } from 'three';

function polarToCartesian(angle: number, radius: number): Vector2 {
    const x: number = Math.cos(angle) * radius;
    const y: number = Math.sin(angle) * radius;
    return new Vector2(x, y);
}

function detectCollision(
    playerPosition: Vector2, 
    playerDirection: Vector2, 
    playerRadius: number,
    objectPosition: Vector2, 
    objectDirection: Vector2, 
    objectRadius: number,
    resolution: number = 1
): boolean {
    const circleCollision = (
        aPos: Vector2, 
        bPos: Vector2, 
        aRad: number, 
        bRad: number
    ): boolean => aPos.distanceTo(bPos) <= aRad + bRad;

    for (let i = 0; i < resolution; i++) {
        const intermediateFrame: number = (1 / resolution) * i;
        const aPos: Vector2 = playerPosition.add(playerDirection.multiplyScalar(intermediateFrame));
        const bPos: Vector2 = objectPosition.add(objectDirection.multiplyScalar(intermediateFrame));
        if (circleCollision(aPos, bPos, playerRadius, objectRadius)) {
            return true;
        }
    }
    return false;
}

export {
    polarToCartesian,
    detectCollision,
};
