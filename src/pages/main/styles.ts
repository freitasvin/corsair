import styled from "styled-components";

export const MainContainer = styled.div`
  background-image: url("/assets/fundo.png");
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  display: flex;
  align-items: center;
  padding: 100px;
  height: 100vh;
`;

export const MainContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const GameTitle = styled.div`
  font-size: xx-large;
  color: #313133;
`;

export const ButtonWrapper = styled.div`
  height: 100%;
`;

export const StartButton = styled.button`
  background: #fffc2e;
  font-family: "Pirata One", sans-serif;
  text-transform: uppercase;
  color: #313133;
  border: none;
  box-shadow: #fffc2ea6;
  cursor: pointer;
  outline: none;
  font-size: 48px;
  min-width: 340px;
  min-height: 90px;
  letter-spacing: 1.3px;
  font-weight: 700;
  border-radius: 50px;
  padding: 10px;
  transition: all 0.3s ease-in-out 0s;

  &:hover {
    transform: translateY(-10px); /* Move o botão para cima em 10px */
  }
`;
