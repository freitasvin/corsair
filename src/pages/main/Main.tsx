import { useNavigate } from "react-router-dom";

import * as S from "./styles";

export function Main() {
  const navigate = useNavigate();

  return (
    <S.MainContainer>
      <S.MainContent>
        <S.GameTitle>
          <h2>Bem vindo ao jogo Corsair!</h2>
        </S.GameTitle>

        <S.ButtonWrapper>
          <S.StartButton onClick={() => navigate("/game")}>Jogar</S.StartButton>
        </S.ButtonWrapper>
      </S.MainContent>
    </S.MainContainer>
  );
}
