import { Canvas } from "@react-three/fiber";
import { GameScene } from "../../components/GameScene";
import { useShipStore } from "../../components/Ship";
import { useGameStore } from "../../components/GameScene/useLogic";
import { hideGameOverModal } from "../../utils/game";

import * as S from "./styles";

function Header() {
  return (
    <S.GameDescription>
      <p>Corsair</p>
      <p>
        Blog <a href="https://manu.ninja/">manu.ninja</a>
      </p>
      <p>
        Twitter <a href="https://twitter.com/manuelwieser">@manuelwieser</a>
      </p>
    </S.GameDescription>
  );
}

function GameOverModal({ onTryAgain }: { onTryAgain: () => void }) {
  return (
    <div id="gameOverModal" className="modal">
      <div className="modal-content">
        <h2>Game Over</h2>
        <p id="gameOverMessage"></p>
        <button className="PlayAgain" onClick={onTryAgain}>
          Jogar novamente
        </button>
      </div>
    </div>
  );
}

export function Game() {
  const hasStarted = useShipStore((state) => state.hasStarted);

  const gameStoreData = useGameStore((state) => ({
    score: state.score,
    record: state.record,
    reset: state.reset,
  }));

  const { score, record, reset } = gameStoreData;

  const tryAgain = () => {
    reset();
    hideGameOverModal();
  };

  const welcomeMessage = !hasStarted && (
    <S.Welcome>
      <S.WelcomeTitle>
        Pressione espaço para começar sua navegação!
      </S.WelcomeTitle>
    </S.Welcome>
  );

  return (
    <S.Game>
      <Header />

      {welcomeMessage}

      <S.GameStatus>
        <S.Score>Score {score}</S.Score>
        <S.Record>Record {record}</S.Record>
      </S.GameStatus>

      <GameOverModal onTryAgain={tryAgain} />

      <div id="locExplosion" />

      <Canvas
        onCreated={({ gl }) => {
          gl.shadowMap.enabled = true;
          gl.setPixelRatio(window.devicePixelRatio);
          gl.setSize(window.innerWidth, window.innerHeight);
        }}
      >
        <perspectiveCamera
          position={[0, 0, 100]}
          fov={75}
          aspect={window.innerWidth / window.innerHeight}
          near={1}
          far={200}
        />
        <GameScene />
      </Canvas>
    </S.Game>
  );
}
