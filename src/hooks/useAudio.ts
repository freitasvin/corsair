import { useEffect, useRef, useState } from "react";

type UseAudioReturnType = [boolean, () => void];

export const useAudio = (audioUrl: string) => {
  const audioRef = useRef<HTMLAudioElement | null>(null);
  const [playing, setPlaying] = useState(false);

  const toggle = () => setPlaying((playing) => !playing);

  useEffect(() => {
    const handleEnded = () => {
      // Quando a música chega ao fim, reinicia a reprodução
      if (audioRef.current) {
        audioRef.current.currentTime = 0;
        audioRef.current.play();
      }
    };

    // Limpa o elemento de áudio anterior
    if (audioRef.current) {
      audioRef.current.pause();
      audioRef.current.removeEventListener("ended", handleEnded);
    }

    // Cria um novo elemento de áudio
    audioRef.current = new Audio(audioUrl);

    // Adiciona o evento de fim (ended)
    audioRef.current.addEventListener("ended", handleEnded);

    // Inicia ou pausa a reprodução conforme o estado atual
    if (playing) {
      audioRef.current.play();
    }

    // Limpa os eventos ao desmontar
    return () => {
      if (audioRef.current) {
        audioRef.current.pause();
        audioRef.current.removeEventListener("ended", handleEnded);
      }
    };
  }, [playing, audioUrl]);

  return [playing, toggle] as UseAudioReturnType;
};
