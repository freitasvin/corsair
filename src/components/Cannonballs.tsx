import { useFrame } from "@react-three/fiber";
import { create } from "zustand";
import { subscribeWithSelector } from "zustand/middleware";
import { useShipStore } from "./Ship";
import { detectCollision, polarToCartesian } from "../utils/helpers";
import { MutableRefObject, useEffect, useRef } from "react";
import {
  BufferGeometry,
  Material,
  Mesh,
  NormalBufferAttributes,
  Object3DEventMap,
} from "three";
import {
  calculateCannonSpeed,
  calculateCannonballSpeed,
  displayGameOverModal,
  explosion,
} from "../utils/game";
import { useGameStore } from "./GameScene/useLogic";
import throttle from "lodash.throttle";
import { shallow } from "zustand/shallow";

type Cannon = {
  speed: number;
};

type Cannonball = {
  angle: number;
  radius: number;
  size: number;
  speed: number;
  ref?: MutableRefObject<Mesh | undefined>;
  setRef: (
    ref: Mesh<
      BufferGeometry<NormalBufferAttributes>,
      Material | Material[],
      Object3DEventMap
    >
  ) => void;
};

type CoinStore = {
  cannonballs: Cannonball[];
  cannon: Cannon;
  addCannonball: (cannonball: Omit<Cannonball, "setRef">) => void;
  updateToColisioned: (index: number) => void;
};

export const useCannonballsStore = create(
  subscribeWithSelector<CoinStore>((set) => ({
    cannonballs: [],
    cannon: {
      speed: calculateCannonSpeed(useGameStore.getState().stage),
    },
    addCannonball: (cannonball) => {
      set((state) => ({
        cannonballs: [
          ...state.cannonballs,
          {
            ...cannonball,
            setRef: (ref) => {
              set(({ cannonballs }) => {
                const newCanonballs = [...cannonballs];
                const currentIndex = newCanonballs.length - 1;

                if (newCanonballs[currentIndex]) {
                  newCanonballs[currentIndex].ref = {
                    current: ref,
                  };
                }

                return { cannonballs: newCanonballs };
              });
            },
          },
        ],
      }));
    },
    updateToColisioned: (index: number) => {
      useCannonballsStore.setState((state) => ({
        cannonballs: state.cannonballs.map((cannonball, i) =>
          i === index
            ? {
                ...cannonball,
                collected: true,
              }
            : cannonball
        ),
      }));
    },
  }))
);

useShipStore.subscribe(
  (state) => state.hasStarted,
  (hasStarted) => {
    !hasStarted && useCannonballsStore.setState({ cannonballs: [] });
    console.log(hasStarted);
  },
  {
    equalityFn: shallow,
  }
);

function cannonballFactory(last: number) {
  let angle;
  do {
    angle = Math.random() * Math.PI * 2;
  } while (angle > last - Math.PI / 2 && angle < last + Math.PI / 2);
  return {
    angle,
    radius: 0,
    size: 2,
    collision: false,
  };
}

export function Cannonballs() {
  const { cannonballs, updateToColisioned } = useCannonballsStore((state) => ({
    cannonballs: state.cannonballs,
    updateToColisioned: state.updateToColisioned,
  }));
  const { hasShipStarted, shipDestroyed } = useShipStore((state) => ({
    shipDestroyed: state.shipDestroyed,
    hasShipStarted: state.hasStarted,
  }));
  const stage = useGameStore((state) => state.stage);

  useEffect(() => {
    if (!hasShipStarted) return;

    const {
      cannon: { speed },
      cannonballs,
      addCannonball,
    } = useCannonballsStore.getState();
    const intervalId = setInterval(
      () =>
        addCannonball({
          ...cannonballFactory(
            cannonballs.length ? cannonballs[cannonballs.length - 1].angle : 0
          ),
          speed: calculateCannonballSpeed(stage),
        }),
      speed
    );

    return () => clearInterval(intervalId);
  }, [hasShipStarted, stage]);

  const updateStateToColisioned = useRef(
    throttle((index: number) => {
      const explosion_sound = new Audio("/assets/sounds/explosion_sound.wav");
      explosion_sound.play();

      updateToColisioned(index);

      useShipStore.setState({ hasStarted: false });

      explosion();

      displayGameOverModal("Você perdeu! Tente novamente.");
    }, 1000)
  ).current;

  useFrame((_, delta) => {
    const {
      angle: playerAngle,
      radius: playerRadius,
      direction,
      size: playerSize,
    } = useShipStore.getState();
    const playerDirection = playerAngle + (Math.PI / 2) * direction;
    const playerSpeed = delta * direction * calculateCannonballSpeed(stage);

    const cannonbals = cannonballs.map((cannonball, i) => {
      const {
        angle: cannonballAngle,
        radius: cannonBallRadius,
        size: cannonBallSize,
      } = cannonball;
      const cannonballSpeed = delta * calculateCannonballSpeed(stage) * 1000;

      const collision = detectCollision(
        polarToCartesian(playerAngle, playerRadius),
        polarToCartesian(playerDirection, playerSpeed),
        playerSize,
        polarToCartesian(cannonballAngle, cannonBallRadius),
        polarToCartesian(cannonballAngle, cannonballSpeed),
        cannonBallSize,
        4
      );

      if (collision) updateStateToColisioned(i);

      if (!hasShipStarted || shipDestroyed || collision) return cannonball;

      const { angle, radius } = cannonball;
      const position = polarToCartesian(angle, radius);

      if (!cannonball.ref?.current) return cannonball;

      cannonball.ref.current.position.x = position.x;
      cannonball.ref.current.position.y = position.y;
      cannonball.ref.current.visible = true;

      return { ...cannonball, radius: cannonBallRadius + cannonballSpeed };
    });

    useCannonballsStore.setState({
      cannonballs: hasShipStarted ? cannonbals : [],
    });
  });

  return (
    <group>
      {cannonballs.map(({ angle, radius, size, setRef }, i) => (
        <mesh
          ref={setRef}
          key={i}
          position={[radius * Math.cos(angle), radius * Math.sin(angle), 0]}
        >
          <sphereGeometry args={[size, 32, 32]} />
          <meshPhongMaterial
            color={0x23232d}
            shininess={64}
            specular={0x646478}
          />
        </mesh>
      ))}
    </group>
  );
}
