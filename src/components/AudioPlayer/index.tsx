import { useRef } from "react";
import { useAudio } from "../../hooks/useAudio";
import { SlVolume2, SlVolumeOff } from "react-icons/sl";

import * as S from "./styles";

const ICON_SIZE = 25;

type PlayerProps = {
  audioUrl: string;
};

export const AudioPlayer = ({ audioUrl }: PlayerProps) => {
  const [playing, toggle] = useAudio(audioUrl);
  const buttonRef = useRef<HTMLButtonElement | null>(null);

  const handleToggleButton = () => {
    buttonRef.current!.blur();
    toggle();
  };

  return (
    <div>
      <S.AudioButton onClick={handleToggleButton} ref={buttonRef}>
        {playing ? (
          <SlVolume2 size={ICON_SIZE} />
        ) : (
          <SlVolumeOff size={ICON_SIZE} />
        )}
      </S.AudioButton>
    </div>
  );
};
