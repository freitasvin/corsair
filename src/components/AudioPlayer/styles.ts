import styled from "styled-components";

export const AudioButton = styled.button`
  background-color: #fffc2e;
  position: absolute;
  border: none;
  width: 80px;
  top: 15px;
  left: 270px;
  padding: 5px;
  border-radius: 25px;
  z-index: 100;
`;
