import { useLoader } from "@react-three/fiber";
import { useEffect, useRef } from "react";
import { DoubleSide, Mesh } from "three";
import { MTLLoader } from "three/examples/jsm/loaders/MTLLoader.js";
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader.js";

export function Island() {
  const islandMtl = useLoader(MTLLoader, "/assets/island.mtl");
  const islandObject = useLoader(OBJLoader, "/assets/island.obj", (loader) => {
    islandMtl.preload();
    loader.setMaterials(islandMtl);
  });
  const islandRef = useRef<Mesh>(null);

  useEffect(() => {
    islandRef.current?.scale.multiplyScalar(32);
    islandRef.current?.rotation.set(Math.PI / 2, 0, 0);
    islandRef.current?.position.set(0, 0, -8.5);

    islandRef.current?.traverse((node) => {
      if (node instanceof Mesh) {
        node.castShadow = true;
        if (node.material) {
          node.material.side = DoubleSide;
        }
      }
    });
  }, []);

  return (
    <mesh ref={islandRef} castShadow>
      <primitive object={islandObject} />
    </mesh>
  );
}
